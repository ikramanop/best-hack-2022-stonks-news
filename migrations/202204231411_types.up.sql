begin;

insert into news_types (category, sources)
values ('Политика', '[
  "ria.ru",
  "tass.ru",
  "lenta.ru",
  "russian.rt.com",
  "rbc.ru",
  "kp.ru"
]');

commit;