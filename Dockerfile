FROM golang:alpine as builder

ENV PATH=$PATH:$GOROOT/bin:$GOPATH/bin

RUN apk add --update --no-cache alpine-sdk bash ca-certificates \
      libressl \
      tar \
      git openssh openssl yajl-dev zlib-dev cyrus-sasl-dev openssl-dev coreutils curl

WORKDIR /src/app
COPY . /src/app

RUN go mod download
RUN go mod vendor

RUN make build

FROM alpine:latest as runner

COPY --from=builder /src/app/bin .
COPY ./migrations ./migrations

ENTRYPOINT [ "./stonks-news" ]
