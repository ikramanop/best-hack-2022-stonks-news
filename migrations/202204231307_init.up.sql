begin;

create table news_types
(
    id       serial primary key not null,
    category text               not null unique,
    sources  json               not null
);

create table news
(
    id           serial primary key not null,
    news_type    text               not null,
    author       text               not null,
    title        text               not null,
    description  text               not null,
    url          text               not null,
    url_to_image text               not null,
    published_at timestamp          not null,
    hash         text unique        not null
);

commit;