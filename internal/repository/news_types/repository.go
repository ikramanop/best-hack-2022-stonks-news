package news_types

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type NewsTypes struct {
	*pgx.Conn
}

func New() (NewsTypes, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_NEWS_DSN"))
	if err != nil {
		return NewsTypes{}, err
	}

	return NewsTypes{db}, nil
}
