package cmd

import (
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/spf13/cobra"
	"os"
)

var runMigrateCMD = &cobra.Command{
	Use:   "migrate",
	Short: "Run migrations",
	Long:  "Run migrations",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runMigrations()
	},
}

func runMigrations() error {
	m, err := migrate.New("file://migrations", os.Getenv("STONKS_NEWS_DSN"))
	if err != nil {
		return err
	}

	err = m.Up()
	if err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			fmt.Println("Migrations up done (no changes)")
			return nil
		}

		return err
	}

	fmt.Println("Migrations up done")

	return nil
}
