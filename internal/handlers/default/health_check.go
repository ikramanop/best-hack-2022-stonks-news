package _default

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/default_operations"
)

func HealthCheckHandler(params default_operations.HealthCheckParams) middleware.Responder {
	log.Printf("Hit GET /health/check from %s\n", params.HTTPRequest.UserAgent())

	return default_operations.NewHealthCheckOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
