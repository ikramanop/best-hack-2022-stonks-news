// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/default_operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/news"
)

//go:generate swagger generate server --target ../../generated --name API --spec ../../../pkg/swagger/server.yaml --principal interface{} --exclude-main

func configureFlags(api *operations.APIAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.APIAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if api.NewsGetCategoriesHandler == nil {
		api.NewsGetCategoriesHandler = news.GetCategoriesHandlerFunc(func(params news.GetCategoriesParams) middleware.Responder {
			return middleware.NotImplemented("operation news.GetCategories has not yet been implemented")
		})
	}
	if api.NewsGetNewsHandler == nil {
		api.NewsGetNewsHandler = news.GetNewsHandlerFunc(func(params news.GetNewsParams) middleware.Responder {
			return middleware.NotImplemented("operation news.GetNews has not yet been implemented")
		})
	}
	if api.DefaultOperationsHealthCheckHandler == nil {
		api.DefaultOperationsHealthCheckHandler = default_operations.HealthCheckHandlerFunc(func(params default_operations.HealthCheckParams) middleware.Responder {
			return middleware.NotImplemented("operation default_operations.HealthCheck has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
