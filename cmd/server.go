package cmd

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/default_operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/news"
	_default "gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/handlers/default"
	news2 "gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/handlers/news"
	"log"
	"os"
	"strconv"

	"github.com/go-openapi/loads"
	"github.com/spf13/cobra"

	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations"
)

var runServerCMD = &cobra.Command{
	Use:   "server",
	Short: "Run server",
	Long:  "Run server",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runServer()
	},
}

func runServer() error {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		return err
	}

	api := operations.NewAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			log.Fatalln(err)
		}
	}()

	port, err := strconv.Atoi(os.Getenv("STONKS_NEWS_PORT"))
	if err != nil {
		return err
	}

	server.Port = port

	api.DefaultOperationsHealthCheckHandler = default_operations.HealthCheckHandlerFunc(_default.HealthCheckHandler)

	api.NewsGetNewsHandler = news.GetNewsHandlerFunc(news2.GetNewsHandler)
	api.NewsGetCategoriesHandler = news.GetCategoriesHandlerFunc(news2.GetCategoriesHandler)

	if err := server.Serve(); err != nil {
		return err
	}

	return nil
}
