package news

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/news"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/repository/news_types"
	"log"

	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/models"
)

func GetCategoriesHandler(params news.GetCategoriesParams) middleware.Responder {
	log.Printf("Hit GET /news/categories from %s\n", params.HTTPRequest.UserAgent())

	rx, err := news_types.New()
	if err != nil {
		return news.NewGetCategoriesBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	typesRaw, ok, err := rx.GetTypes()
	if err != nil {
		return news.NewGetNewsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return news.NewGetNewsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "news not found",
				Status:  false,
			},
		)
	}

	var categoriesResponse []*models.Category
	for _, typeRaw := range typesRaw {
		categoriesResponse = append(categoriesResponse, &models.Category{Tag: typeRaw.Category})
	}

	return news.NewGetCategoriesOK().WithPayload(
		&models.Categories{Categories: categoriesResponse},
	)
}
