package news

import (
	"context"

	"github.com/robtec/newsapi/api"
)

func (r *News) AddNews(newsType string, news api.Article, hash string) (bool, error) {
	query := `
insert into news (news_type, author, title, description, url, url_to_image,
published_at, hash)
values ($1, $2, $3, $4, $5, $6, $7, $8)
`

	_, err := r.Exec(context.Background(), query, newsType, news.Author, news.Title, news.Description,
		news.URL, news.URLToImage, news.PublishedAt, hash)
	if err != nil {
		return false, err
	}

	return true, nil
}
