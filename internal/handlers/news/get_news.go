package news

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/restapi/operations/news"
	"log"
	"time"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/generated/models"
	news2 "gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/repository/news"
)

func GetNewsHandler(params news.GetNewsParams) middleware.Responder {
	log.Printf("Hit POST /news/get from %s\n", params.HTTPRequest.UserAgent())

	rx, err := news2.New()
	if err != nil {
		return news.NewGetNewsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	newsRaw, ok, err := rx.GetNewsByTypes(params.Body.Tags)
	if err != nil {
		return news.NewGetNewsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return news.NewGetNewsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "news not found",
				Status:  false,
			},
		)
	}

	var newsResponses []*models.NewsSingle
	for _, newsRawS := range newsRaw {
		newsResponses = append(newsResponses, &models.NewsSingle{
			Author:      newsRawS.Author,
			Description: newsRawS.Description,
			PublishedAt: newsRawS.PublishedAt.Format(time.RFC3339),
			Title:       newsRawS.Title,
			URL:         newsRawS.URL,
			URLToImage:  newsRawS.URLToImage,
		})
	}

	return news.NewGetNewsOK().WithPayload(
		&models.NewsResponse{News: newsResponses},
	)
}
