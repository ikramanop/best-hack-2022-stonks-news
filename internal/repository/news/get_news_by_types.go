package news

import (
	"context"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/model"
)

func (r *News) GetNewsByTypes(newsTypes []string) ([]model.News, bool, error) {
	query := `
select id, news_type, author, title, description, url, url_to_image,
published_at, hash
from news
where news_type = any ($1)
`

	rows, err := r.Query(context.Background(), query, newsTypes)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	var news []model.News
	for rows.Next() {
		var singleNews model.News
		err := rows.Scan(&singleNews.ID, &singleNews.NewsType, &singleNews.Author,
			&singleNews.Title, &singleNews.Description, &singleNews.URL,
			&singleNews.URLToImage, &singleNews.PublishedAt, &singleNews.Hash)
		if err != nil {
			return nil, false, err
		}
		news = append(news, singleNews)
	}

	return news, true, nil
}
