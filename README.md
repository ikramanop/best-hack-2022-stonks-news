# BEST HACK 2022 Stonks News

## Сервис для работы с новостным API

Stonks News хранит и изменяет информацию о пользователях, авторизирует и аутентифицирует их.

## Функционал

- Получение новостей по ключевым словам
- Конфигурирование парсинга новостей с внешнего API

## Cтек

- Golang
- Swagger OpenAPI
- PostgreSQL

## Как поднять?

Создайте docker-сеть

```bash
docker network create stonks-network
```

Поднимите приложение со всеми зависимостями

```bash
make run-app
```

Завершите работу приложения

```bash
make stop-app
```

Является частью полноценной биржевой системы Stonks

## Как пользоваться?

В рамках "большого" API [Stonks API] https://gitlab.com/ikramanop/best-hack-2022-stonks-api