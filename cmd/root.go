package cmd

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "best-hack-2022-stonks-api",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	err := godotenv.Load("./deployment/local.env")
	if err != nil {
		fmt.Println(err)
	}

	rootCmd.AddCommand(runMigrateCMD)
	rootCmd.AddCommand(runServerCMD)
	rootCmd.AddCommand(runWorkerCMD)
}
