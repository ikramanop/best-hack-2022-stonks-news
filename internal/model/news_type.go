package model

type NewsType struct {
	ID uint64 `db:"id"`

	Category string `db:"category"`

	Sources []string `db:"sources"`
}
