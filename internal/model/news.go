package model

import "time"

type News struct {
	ID uint64 `db:"id"`

	NewsType string `db:"news_type"`

	Author string `db:"author"`

	Title string `db:"title"`

	Description string `db:"description"`

	URL string `db:"url"`

	URLToImage string `db:"url_to_image"`

	PublishedAt time.Time `db:"published_at"`

	Hash string `db:"hash"`
}
