package news_types

import (
	"context"

	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/model"
)

func (r *NewsTypes) GetTypes() ([]model.NewsType, bool, error) {
	query := `
select id, category, sources
from news_types
`

	var newsTypes []model.NewsType
	rows, err := r.Query(context.Background(), query)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var newsType model.NewsType
		err := rows.
			Scan(&newsType.ID, &newsType.Category, &newsType.Sources)
		if err != nil {
			return nil, false, err
		}
		newsTypes = append(newsTypes, newsType)
	}

	return newsTypes, true, nil
}
