pkgs  = $(shell GOFLAGS=-mod=mod go list ./... | grep -vE -e /vendor/ -e /pkg/swagger/)
SWAGGER=server.yaml
BINARY=stonks-news

swagger-validate:
	@echo "==> validating swagger declaration"
	swagger validate "${PWD}/pkg/swagger/${SWAGGER}"

swagger-doc:
	@echo "==> generating swagger doc"
	rm -rf ./doc
	mkdir ./doc
	python3 ./pkg/swagger/swagger-yaml-to-html.py < ./pkg/swagger/${SWAGGER} > ./doc/index.html

swagger-gen:
	@echo "==> generating swagger go code"
	rm -rf ./internal/generated
	GOFLAGS=-mod=mod go generate ${pkgs}

build:
	@echo "==> building application"
	rm -rf bin
	go build -tags dynamic -o bin/${BINARY} main.go

run-server:
	@echo "==> running application"
	make build
	./bin/${BINARY} server

run-migrations:
	@echo "==> running migrations"
	make build
	./bin/${BINARY} migrate

run-worker:
	@echo "==> running worker"
	make build
	./bin/${BINARY} worker

run-db:
	@echo "==> running database in docker"
	docker-compose -p ${BINARY} -f deployment/docker-compose.yaml up --build -d ${BINARY}-db

stop-app:
	@echo "==> stopping app"
	docker-compose -p ${BINARY} -f deployment/docker-compose.yaml down --remove-orphans

run-app:
	@echo "==> running app"
	docker-compose -p ${BINARY} -f deployment/docker-compose.yaml up --build -d ${BINARY}