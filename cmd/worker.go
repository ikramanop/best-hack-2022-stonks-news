package cmd

import (
	"github.com/mitchellh/hashstructure/v2"
	"github.com/robfig/cron/v3"
	"github.com/robtec/newsapi/api"
	"github.com/spf13/cobra"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/repository/news"
	"gitlab.com/ikramanop/best-hack-2022-stonks-news/internal/repository/news_types"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

var runWorkerCMD = &cobra.Command{
	Use:   "worker",
	Short: "Run worker",
	Long:  "Run worker",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runWorker()
	},
}

func runWorker() error {
	c := cron.New()
	_, err := c.AddFunc(os.Getenv("NEWS_UPDATE_CRON_INTERVAL"), updateNews)
	if err != nil {
		return err
	}
	updateNews()
	go c.Start()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig

	return nil
}

func updateNews() {
	httpClient := http.Client{}
	key := os.Getenv("NEWS_API_KEY")
	url := "https://newsapi.org"

	rxnt, err := news_types.New()
	if err != nil {
		log.Println(err)
		return
	}

	types, ok, err := rxnt.GetTypes()
	if err != nil {
		log.Println(err)
		return
	}
	if !ok {
		log.Println("no news types in db")
		return
	}

	client, err := api.New(&httpClient, key, url)
	if err != nil {
		log.Println(err)
		return
	}

	rxn, err := news.New()
	if err != nil {
		log.Println(err)
		return
	}

	for _, newsType := range types {
		opts := api.Options{
			Domains:  sourcesString(newsType.Sources),
			From:     time.Now().AddDate(0, 0, -7).Format(time.RFC3339),
			Language: "ru",
			SortBy:   "relevancy",
		}

		everything, err := client.Everything(opts)
		if err != nil {
			log.Println(err)
			return
		}

		for _, a := range everything.Articles {
			hash, err := hashstructure.Hash(a, hashstructure.FormatV2, nil)
			if err != nil {
				log.Println(err)
				continue
			}

			ok, err := rxn.AddNews(newsType.Category, a, strconv.FormatUint(hash, 10))
			if err != nil {
				log.Println(err)
				return
			}
			if !ok {
				log.Println("no news types in db")
				return
			}
		}
	}
}

func sourcesString(sources []string) string {
	result := ""
	for i, s := range sources {
		result += s
		if i != len(sources)-1 {
			result += ","
		}
	}

	return result
}
