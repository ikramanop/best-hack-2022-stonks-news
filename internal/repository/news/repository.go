package news

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type News struct {
	*pgx.Conn
}

func New() (News, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_NEWS_DSN"))
	if err != nil {
		return News{}, err
	}

	return News{db}, nil
}
